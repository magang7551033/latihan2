import 'package:flutter/material.dart';
import 'package:latihan_2/read_note_page.dart';

import '../util.dart';

class Note {
  // Fields
  String? title;
  String? content;
  Color backgroundColor;

  // Constructor
  Note(this.title, this.content, this.backgroundColor);

  // Builder function that output note card
  Widget buildNoteCard(BuildContext context, int noteIndex,
      Function(Note) removeNoteFunction, Function(Note, int) editNoteFunction) {
    List<Text> noteTextList = getNoteTextList(true);

    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return NoteDetailPage(note: this);
        }));
      },
      onLongPress: () => _showActionBottomSheet(
          context, noteIndex, removeNoteFunction, editNoteFunction),
      child: Card(
        color: backgroundColor == Colors.white ? null : backgroundColor,
        child: Container(
          margin: const EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: noteTextList,
          ),
        ),
      ),
    );
  }

  // Reusable function that create a list of Text widget containing
  // Title and content of the note
  List<Text> getNoteTextList(bool wrapOnOverflow) {
    List<Text> noteTextList = [];
    Color textColor = Util.calculateTextColor(backgroundColor);
    if (title != null) {
      noteTextList.add(Text(
        title!,
        style: Util.getTitleStyle(textColor),
        maxLines: wrapOnOverflow ? 2 : null,
        overflow: wrapOnOverflow ? TextOverflow.ellipsis : null,
      ));
    }
    if (content != null) {
      noteTextList.add(Text(
        content!,
        style: Util.getContentStyle(textColor),
        maxLines: wrapOnOverflow ? 6 : null,
        overflow: wrapOnOverflow ? TextOverflow.ellipsis : null,
      ));
    }
    return noteTextList;
  }

  void _showActionBottomSheet(BuildContext context, int noteIndex,
      Function(Note) removeNoteFunction, Function(Note, int) editNoteFunction) {
    showModalBottomSheet<void>(
        context: context,
        showDragHandle: true,
        builder: (BuildContext context) {
          return Container(
            margin: const EdgeInsets.all(15),
            child: ListTile(
              title: const Text('Actions'),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 15),
                    child: CircleAvatar(
                      child: IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () {
                          removeNoteFunction(this);
                        },
                      ),
                    ),
                  ),
                  CircleAvatar(
                    child: IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () {
                        Navigator.pop(context);
                        editNoteFunction(this, noteIndex);
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
