import 'package:flutter/material.dart';
import 'package:latihan_2/create_update_note_page.dart';
import 'package:latihan_2/data/note.dart';
import 'package:latihan_2/util.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Latihan 2',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.lightGreen),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<Note> _noteList = [];
  bool _isGridMode = false;

  void _removeNote(Note note) {
    setState(() {
      _noteList.remove(note);
    });
    Navigator.pop(context);
    Util.showSnackBar(context, 'Note successfully deleted!');
  }

  void _addNote(Note newNote) {
    setState(() {
      _noteList.add(newNote);
    });
    Util.showSnackBar(context, 'Note created successfully!');
    // ScaffoldMessenger.of(context).showSnackBar(
    //     const SnackBar(content: Text('Note created successfully!')));
  }

  void _showNewNotePage() async {
    // Show note form page and wait for result
    // If note form page return Note object then newNote will not be null
    Note? newNote = await Navigator.push(context,
        MaterialPageRoute<Note>(builder: (BuildContext context) {
      return const NoteFormPage.createNew();
    }));
    if (newNote != null) {
      // If user created a note then add it to the list
      _addNote(newNote);
    }
  }

  void _replaceNote(Note replacementNote, int replacedIndex) {
    setState(() {
      _noteList[replacedIndex] = replacementNote;
    });
    Util.showSnackBar(context, 'Note successfully modified!');
  }

  void _showUpdateNotePage(Note oldNote, int replacedIndex) async {
    // Show note form page with note content pre filled on the form
    // If note form page return Note object then user decided to edit the note
    // If user backs out then replacementNote will be null
    Note? replacementNote = await Navigator.push(context,
        MaterialPageRoute<Note>(builder: (BuildContext context) {
      return NoteFormPage(
        oldTitle: oldNote.title ?? '',
        oldContent: oldNote.content ?? '',
        oldColor: oldNote.backgroundColor,
      );
    }));
    if (replacementNote != null) {
      _replaceNote(replacementNote, replacedIndex);
    }
  }

  void _changeNoteLayout() {
    setState(() {
      _isGridMode = !_isGridMode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Notes'),
        actions: _noteList.isEmpty // Show actions only if note list not empty
            ? null
            : [
                IconButton(
                    onPressed: _changeNoteLayout,
                    icon: Icon(_isGridMode ? Icons.grid_view : Icons.list))
              ],
      ),
      // Body will show text when there is no note yet
      body: _noteList.isEmpty
          ? const Center(child: Text('Make your first note'))
          : _noteViewBuilder(),
      floatingActionButton: FloatingActionButton(
        onPressed: _showNewNotePage,
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _noteViewBuilder() {
    // If grid mode is true then return note list in grid view
    // Otherwise return note list in list view
    if (_isGridMode) {
      return GridView.builder(
          padding: const EdgeInsets.all(20),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2),
          itemCount: _noteList.length,
          itemBuilder: ((BuildContext context, int index) {
            return _noteList[index].buildNoteCard(
                context, index, _removeNote, _showUpdateNotePage);
          }));
    } else {
      return ListView.separated(
          padding: const EdgeInsets.all(20),
          itemCount: _noteList.length,
          itemBuilder: ((BuildContext context, int index) {
            return _noteList[index].buildNoteCard(
                context, index, _removeNote, _showUpdateNotePage);
          }),
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(color: Colors.transparent, height: 8));
    }
  }
}
