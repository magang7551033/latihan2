import 'package:flutter/material.dart';

import 'data/note.dart';

class NoteDetailPage extends StatelessWidget {
  const NoteDetailPage({super.key, required Note note}) : _note = note;

  final Note _note;

  @override
  Widget build(BuildContext context) {
    List<Text> noteTextList = _note.getNoteTextList(false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Note Details'),
      ),
      body: Container(
        color: _note.backgroundColor,
        child: ListView.separated(
            padding: const EdgeInsets.all(20),
            itemCount: noteTextList.length,
            itemBuilder: ((BuildContext context, int index) {
              return noteTextList[index];
            }),
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(color: Colors.transparent, height: 15)),
      ),
    );
  }
}
