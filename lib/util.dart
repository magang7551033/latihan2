import 'package:flutter/material.dart';

// Util class containing reusable function for ease of use
class Util {
  static Color calculateTextColor(Color color) {
    // Calculate the luminance of the background color
    double luminance = color.computeLuminance();

    // Determine whether to use light or dark text based on luminance
    return luminance > 0.5 ? Colors.black : Colors.white;
  }

  static TextStyle getTitleStyle(Color textColor) {
    // Note title text style preset
    return TextStyle(
        color: textColor, fontSize: 17, fontWeight: FontWeight.bold);
  }

  static TextStyle getContentStyle(Color textColor) {
    // Note content text style preset
    return TextStyle(color: textColor, fontWeight: FontWeight.w400);
  }

  static void showSnackBar(BuildContext context, String text) {
    // showSnackBar boilerplate shortener
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }
}
