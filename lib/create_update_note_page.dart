import 'package:flutter/material.dart';
import 'package:latihan_2/data/note.dart';
import 'package:latihan_2/util.dart';

class NoteFormPage extends StatefulWidget {
  const NoteFormPage(
      {super.key,
      required String oldTitle,
      required String oldContent,
      required Color oldColor})
      : _oldContent = oldContent,
        _oldTitle = oldTitle,
        _oldColor = oldColor;
  const NoteFormPage.createNew({super.key})
      : _oldTitle = '',
        _oldContent = '',
        _oldColor = Colors.white;

  final String _oldTitle;
  final String _oldContent;
  final Color _oldColor;

  @override
  State<NoteFormPage> createState() => _NoteFormPageState();
}

class _NoteFormPageState extends State<NoteFormPage> {
  // For form validation
  final _formKey = GlobalKey<FormState>();
  // For retrieving the text that user has inputted
  final TextEditingController _titleFieldController = TextEditingController();
  final TextEditingController _contentFieldController = TextEditingController();

  // Note color settings
  Color _backgroundColor = Colors.white;

  // App Bar Title (Will be set on widget init at initState())
  String _appBarTitle = '';

  void _changeNoteColor(Color color) {
    setState(() {
      _backgroundColor = color;
    });
  }

  @override
  void initState() {
    super.initState();
    // Set initial text
    _titleFieldController.text = widget._oldTitle;
    _contentFieldController.text = widget._oldContent;
    // Set initial background color
    _backgroundColor = widget._oldColor;
    // Set correct AppBar Title
    _appBarTitle = widget._oldTitle.isEmpty && widget._oldContent.isEmpty
        ? 'Create Note'
        : 'Edit Note';
  }

  @override
  Widget build(BuildContext context) {
    // Get correct text color based on background color
    Color textColor = Util.calculateTextColor(_backgroundColor);
    // Text style for text form
    var titleStyle = Util.getTitleStyle(textColor);
    var noteContentStyle = Util.getContentStyle(textColor);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: Text(_appBarTitle),
        ),
        body: Container(
          color: _backgroundColor,
          child: Form(
            key: _formKey,
            child: Container(
              margin: const EdgeInsets.all(20),
              child: ListView(children: [
                TextFormField(
                  controller: _titleFieldController,
                  maxLines: null,
                  style: titleStyle,
                  decoration: InputDecoration(
                      hintText: 'Title',
                      hintStyle: titleStyle,
                      border: InputBorder.none),
                ),
                TextFormField(
                  controller: _contentFieldController,
                  maxLines: null,
                  style: noteContentStyle,
                  decoration: InputDecoration(
                      hintText: 'Write something here...',
                      hintStyle: noteContentStyle,
                      border: InputBorder.none),
                )
              ]),
            ),
          ),
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: SizedBox(
                width: 45,
                height: 45,
                child: FloatingActionButton(
                  onPressed: () {
                    _showColorBottomSheet(context);
                  },
                  heroTag: null,
                  child: const Icon(Icons.palette),
                ),
              ),
            ),
            FloatingActionButton(
              onPressed: _createNote,
              heroTag: null,
              child: const Icon(Icons.save),
            ),
          ],
        ));
  }

  void _createNote() {
    // Get user input
    String? noteTitle =
        _titleFieldController.text.isEmpty ? null : _titleFieldController.text;
    String? noteContent = _contentFieldController.text.isEmpty
        ? null
        : _contentFieldController.text;
    // Create the note if validaion successfull
    if (noteTitle != null || noteContent != null) {
      Navigator.pop(context, Note(noteTitle, noteContent, _backgroundColor));
    }
  }

  void _showColorBottomSheet(BuildContext context) {
    showModalBottomSheet<void>(
        context: context,
        showDragHandle: true,
        builder: (BuildContext context) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _ColorListTile(
                    color: Colors.amber,
                    colorName: 'Amber',
                    handleTap: _changeNoteColor),
                _ColorListTile(
                    color: Colors.lime,
                    colorName: 'Lime',
                    handleTap: _changeNoteColor),
                _ColorListTile(
                    color: Colors.teal,
                    colorName: 'Teal',
                    handleTap: _changeNoteColor),
                _ColorListTile(
                    color: Colors.indigo,
                    colorName: 'Indigo',
                    handleTap: _changeNoteColor),
                _ColorListTile(
                    color: Colors.brown,
                    colorName: 'Brown',
                    handleTap: _changeNoteColor),
                _ColorListTile(
                    color: Colors.black,
                    colorName: 'Black',
                    handleTap: _changeNoteColor),
                _ColorListTile(
                    color: Colors.white,
                    colorName: 'None',
                    handleTap: _changeNoteColor)
              ]);
        });
  }
}

class _ColorListTile extends StatelessWidget {
  // Reusable widget for color selector
  final Color color;
  final String colorName;
  final void Function(Color) handleTap;

  const _ColorListTile({
    required this.color,
    required this.colorName,
    required this.handleTap,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CircleAvatar(
        backgroundColor: color,
      ),
      title: Text(
        colorName,
        style: const TextStyle(fontSize: 18),
      ),
      onTap: () {
        handleTap(color);
        Navigator.pop(context);
      },
    );
  }
}
